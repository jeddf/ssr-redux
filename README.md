# SSR React Example!

## This is a basic react ssr setup with client side hydration.

### run

`npm install`

`npm run build`

`node server.js`

Missing:

- CSS infrastructure (styled-jsx?)
- hash integrity checks for scripts
