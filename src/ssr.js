import React from "react";
import { renderToString } from "react-dom/server";
import { createStore } from "redux";
import { Provider } from "react-redux";
import appReducer from "./reducer";
import App from "./app";
module.exports = function render() {
  const store = createStore(appReducer);
  return {
    html: renderToString(
      <Provider store={store}>
        <App />
      </Provider>
    ),
    preloadedState: store.getState()
  };
};
