import { connect } from "react-redux";
import { increment, decrement } from "../actions";
import Counter from "../components/Counter";

const mapStateToProps = state => ({
  value: state
});

const mapDispatchToProps = dispatch => ({
  onIncrement: id => dispatch(increment(id)),
  onDecrement: id => dispatch(decrement(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);
