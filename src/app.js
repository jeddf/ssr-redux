const React = require("react");
const { Component } = React;
import Counter from "./containers/Counter";

class App extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.setState({ mounted3SecondsAgo: true });
    }, 3000);
  }

  render() {
    return (
      <div>
        <h1>
          Hello{" "}
          {this.state && this.state.mounted3SecondsAgo
            ? "Browser Human!"
            : "Server Robot!"}
        </h1>
        <Counter />
      </div>
    );
  }
}

export default App;
