const ssr = require("./build/ssr.js");
const serverView = require("./server-view");
const fs = require("fs");

const content = ssr();
const htmlDocString = serverView(content);
fs.writeFileSync("./assets/index.html", htmlDocString);
