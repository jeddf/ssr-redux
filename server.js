const express = require("express");
const app = express();
const ssr = require("./build/ssr.js");
const serverView = require("./server-view");
const path = require("path");

// Serving static files
app.use(express.static(path.resolve(__dirname, "assets")));

app.use((req, res) => {
  const { html, preloadedState } = ssr();
  const response = serverView(html, preloadedState);
  res.send(response);
});

app.listen(3000, () => {
  console.log("listening on port 3000");
});
